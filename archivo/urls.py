from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls.static import static

from api import setting

urlpatterns = \
    [url(r'^', include('api.urls'))] + \
    static(setting.MEDIA_URL, document_root=setting.MEDIA_ROOT) + \
    static(setting.STATIC_URL, document_root=setting.STATIC_ROOT)
