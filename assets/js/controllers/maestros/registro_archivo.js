angular.module('archivo.registro', []).controller('controller_archivo', ['$scope', '$http', '$location', '$bootbox', '$modal',
        function ($scope, $http, $location, $bootbox, $modal) {

        /** para llamar al procedimiento almacenado **/
        var accion=true;
        $scope.texto='Agregar Registro';
        $scope.PAlmacenados={
                listatipos: [],
                id_archivo:'',
                anio:'',
                mes:'',
                dia:'',
                otorgantes:'',
                otorgados:'',
                cla_contrato:'',
                nro:'',
                condicion:'',
                buscar_condi:'',
                provincia: '',
                notario : '',
                pagina : 1,
                get: function () {
                    $scope.maint('archivo/procedimientoalmacenado/get/',
                        {   
                            id_archivo:$scope.PAlmacenados.id_archivo, 
                            anio:$scope.PAlmacenados.anio, 
                            mes:$scope.PAlmacenados.mes,
                            dia:$scope.PAlmacenados.dia,
                            otorgantes:$scope.PAlmacenados.otorgantes,
                            otorgados:$scope.PAlmacenados.otorgados,
                            cla_contrato:$scope.PAlmacenados.cla_contrato,
                            nro:$scope.PAlmacenados.nro, 
                            condicion:$scope.PAlmacenados.condicion,
                            buscar_condi:$scope.PAlmacenados.buscar_condi, 
                            pagina : $scope.PAlmacenados.pagina,
                            provincia : $scope.PAlmacenados.provincia,
                            notario : $scope.PAlmacenados.notario
                        },
                        function (response) {
                            $scope.PAlmacenados.listatipos = response.data.data;
                            var error = response.data.data[0].bdcoderror;
                            if (error==0){
                                mensajeBD(error,response.data.data[0].bdmensaje);
                                $scope.PAlmacenados.btnAgregar();ocultar();
                            }
                        },
                        function (callError) {
                            $scope.openModalOk(callError.data.msg);
                        }, 'r')
                },
                btnAgregar:function () {
                    $scope.texto='Agregar Registro';accion=true;
                    $scope.PAlmacenados.id_archivo='';
                    $scope.PAlmacenados.anio='';
                    $scope.PAlmacenados.mes='';
                    $scope.PAlmacenados.dia='';
                    $scope.PAlmacenados.otorgantes='';
                    $scope.PAlmacenados.otorgados='';
                    $scope.PAlmacenados.cla_contrato='';
                    $scope.PAlmacenados.nro='';
                    $scope.PAlmacenados.provincia='';
                    $scope.PAlmacenados.notario='';
                    $scope.PAlmacenados.condicion='insertar';
                    $scope.PAlmacenados.buscar_condi='';cance(0);
                },
                btnModificar:function () {
                    $scope.texto='Modificar Registro';accion=false;
                },
                agregar:function () {
                    if(accion==true){
                        $scope.PAlmacenados.condicion="insertar";
                        $scope.PAlmacenados.get();
                    }
                    else {
                        $scope.PAlmacenados.condicion="modificar";
                        $scope.PAlmacenados.get();
                    }
                },
                modificar:function (item) {
                    $scope.PAlmacenados.id_archivo=item.id_archivo;
                    $scope.PAlmacenados.anio=item.anio;
                    $scope.PAlmacenados.mes=item.mes;
                    $scope.PAlmacenados.dia=parseInt(item.dia);
                    $scope.PAlmacenados.otorgantes=item.otorgantes;
                    $scope.PAlmacenados.otorgados=item.otorgados;
                    $scope.PAlmacenados.cla_contrato=item.cla_contrato;
                    $scope.PAlmacenados.nro=item.Nro;
                    $scope.PAlmacenados.notario=item.notario;
                    $scope.PAlmacenados.provincia=item.provincia;
                    $scope.PAlmacenados.btnModificar();cance(1);
                },
                eliminar:function (item) {
                    if(confirm('Esta Seguro de eliminar este registro ?')){
                        $scope.PAlmacenados.id_archivo=item.id_archivo;
                        $scope.PAlmacenados.condicion="eliminar";
                        $scope.PAlmacenados.get();
                    }
                },
                listar:function () {
                    $scope.PAlmacenados.condicion="lista";
                },
                buscarbd:function () {
                    $scope.PAlmacenados.condicion="buscar";
                    $scope.PAlmacenados.get();
                }
        };

        $scope.pagina_inicio = function()
        {
            $scope.PAlmacenados.pagina = 1;
        }
        $scope.pagina_atras = function()
        {
            if ($scope.PAlmacenados.pagina > 1)
            {
                $scope.PAlmacenados.pagina--;
                $scope.PAlmacenados.buscarbd();
            }
        }
        $scope.pagina_siguiente = function()
        {
            $scope.PAlmacenados.pagina++;
            $scope.PAlmacenados.buscarbd();
        }

        /** Lista al cargar la pagina **/
        $scope.PAlmacenados.listar();
        $scope.PAlmacenados.get();
        /** Funcion para abrir registara **/
        function cance(id) {
            if(id==0){/** agregar **/
                $('#btncancelar').hide(200);
            }
            else {/** modificar **/
                $('#btncancelar').show(200);
            }
        }cance(0);
        function mensajeBD(id,men) {
            if(id==0){
              $('#mensaje').toggle('');
              $('#mensaje').css('color','blue');
              $('#mensaje').html(men);
            }else {
                $('#mensaje').toggle('');
                $('#mensaje').css('color','red');
                $('#mensaje').html(men);
            }
        }
        function oculta() {$('#mensaje').toggle('');}
        function ocultar() {setTimeout(oculta,3000);}

        $("#buscarentabla").keyup(function () {
            if( $(this).val() != ""){
              $("#contenido_categoria div").hide();
              $("#contenido_categoria span:contains-ci('" + $(this).val() + "')").parent("div").fadeIn();
            }
            else{
              $("#contenido_categoria div").fadeIn();

            }
        });
        $.extend($.expr[":"], {
              "contains-ci": function(elem, i, match, array) {
                  return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
              }
        });
    }]);