angular.module('archivo.busqueda', []).controller('controller_archivo_busqueda', ['$scope', '$http', '$location', '$bootbox', '$modal',
        function ($scope, $http, $location, $bootbox, $modal) {
                $scope.busqueda = {};
                $scope.busqueda.lista = [];
                $scope.fecha_inicial = function()
                {
                        return "1800-01-01";
                }
                $scope.fecha_final = function()
                {
                        let fecha = new Date();
                        let Dia = fecha.getDate();
                        let Mes = fecha.getMonth() + 1;
        
                        if (Mes < 10)
                            Mes = '0' + Mes;
        
                        if (Dia < 10)
                            Dia = '0' + Dia;
        
                        return fecha.getFullYear() + "-" + (Mes) + "-01";  
                }
                $scope.realizar_busqueda = function() {
                        $scope.maint('archivo/busqueda_archivos/get/',
                        {       
                                fecha_inicial : $scope.busqueda.fecha_inicial, 
                                fecha_final : $scope.busqueda.fecha_final, 
                                nombres : $scope.busqueda.nombres,
                                provincia : $scope.busqueda.provincia,
                                pagina : $scope.busqueda.pagina
                        },
                        function (response) {
                            $scope.busqueda.lista = response.data.data;
                        },
                        function (callError) {
                            $scope.openModalOk(callError.data.msg);
                        }, 'r')
                }
                $scope.pagina_inicio = function()
                {
                        $scope.busqueda.pagina = 1;
                        $scope.realizar_busqueda();
                }
                $scope.pagina_atras = function()
                {
                        if ($scope.busqueda.pagina > 1)
                        {
                                $scope.busqueda.pagina--;
                                $scope.realizar_busqueda();
                        }
                }
                $scope.pagina_siguiente = function()
                {
                        $scope.busqueda.pagina++;
                        $scope.realizar_busqueda();
                }
                $scope.ini = function()
                {
                        $scope.busqueda.fecha_inicial = $scope.fecha_inicial();
                        $scope.busqueda.fecha_final = $scope.fecha_final();
                        $scope.busqueda.nombres = '';
                        $scope.busqueda.provincia = '';
                        $scope.busqueda.pagina = 1;
                        $scope.busqueda.lista = [];
                        $scope.realizar_busqueda();
                }
                $scope.ini();
        }]);