from django.conf.urls import url
from django.conf.urls.static import static

from api import setting
from api.services import prestador
from api.services import rrhh
from api.services import tipo

# urlpatterns += (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': setting.MEDIA_ROOT})

urlpatterns = [
                  url(r'^listar/get/$', tipo.listararchivos),
                  url(r'^procedimientoalmacenado/get/$', tipo.palmacenado),
                  url(r'^busqueda_archivos/get/$', tipo.busqueda_archivos)

              ] + static(setting.MEDIA_URL, document_root=setting.MEDIA_ROOT) + \
              static(setting.STATIC_URL,
                     document_root=setting.STATIC_ROOT)
