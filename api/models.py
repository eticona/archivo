from django.db import models
from willaq.api.models import Base
from datetime import datetime

class tipo(Base):
    id_tipo = models.AutoField(primary_key=True)
    tema = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=200)
    estado = models.IntegerField()

    class Meta:
        db_table = "tipo"



