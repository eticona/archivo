import os

from django.core.files.storage import FileSystemStorage

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

STATIC_URL = 'assets/'
STATIC_ROOT = BASE_DIR + "/assets/"

MEDIA_ROOT = BASE_DIR + "/media/"
MEDIA_URL = "/media/"

fs = FileSystemStorage(location=MEDIA_ROOT)
FILES_ROOT = STATIC_ROOT + "pdf/"
tdr_fs = FileSystemStorage(location=FILES_ROOT + "tdr/")