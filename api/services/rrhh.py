from willaq.api.utils import getData
from willaq.api.utils import Rpta
from willaq.api.utils import authToken
from willaq.api.utils import UtilBase

import traceback 
from django.views.decorators.csrf import csrf_exempt
from api.models import tipo

"""---------------------- GET incidencia -----------------------"""
from django.http import HttpResponse
from requests import post


@csrf_exempt
def reporte_datos(request):
    try:
        q = getData(tipo)
        data = q.setRequest(request)
        tipo_ = data["tipo"]
        fecha_inicial = data["fecha_inicial"]
        fecha_final = data["fecha_final"]
        
        lista_datos_ = q.querySPArray("sp_tesoreria_recibo_honorario_reporte", [tipo_, fecha_inicial, fecha_final])
        return Rpta(True, "OK", lista_datos_).send()
    except:
        tb = traceback.format_exc()
        print(tb)
        r = Rpta(False, "Ocurrió un error al realizar la consulta de lista de tipos: " + tb, None)
        return r.send()