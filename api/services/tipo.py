from django.views.decorators.csrf import csrf_exempt
from willaq.api.utils import Response
from willaq.api.utils import UtilBase
from api.models import tipo
import traceback
from willaq.api.utils import Rpta
from willaq.api.utils import getData



@csrf_exempt
def listararchivos(request):
    try:
        q = getData(tipo)
        data = q.setRequest(request)
        lista_tipo_ = q.querySPArray("p_lista", [])
        return Rpta(True, "OK", lista_tipo_).send()
    except:
        tb = traceback.format_exc()
        print(tb)
        r = Rpta(False, "Ocurrió un error al realizar la consulta de lista de tipos: " + tb, None)
        return r.send()

@csrf_exempt
def palmacenado(request):
    try:
        q = getData(tipo)
        data = q.setRequest(request)
        lista_tipo_ = q.querySPArray("p_archivo", [data["id_archivo"],
                                                    data["anio"],
                                                    data["mes"],
                                                    data["dia"],
                                                    data["otorgantes"],
                                                    data["otorgados"],
                                                    data["cla_contrato"],
                                                    data["nro"],
                                                    data["condicion"],
                                                    data["buscar_condi"],
                                                    data["pagina"],
                                                    data["provincia"],
                                                    data["notario"]])
        return Rpta(True, "OK", lista_tipo_).send()

    except:
        tb = traceback.format_exc()
        print(tb)
        r = Rpta(False, "Ocurrió un error al realizar la consulta de lista de tipos: " + tb, None)
        return r.send()

@csrf_exempt
def busqueda_archivos(request):
    try:
        q = getData(tipo)
        data = q.setRequest(request)
        lista_tipo_ = q.querySPArray("sp_archivo_registro_busqueda", [data["fecha_inicial"], data["fecha_final"], data["nombres"], data["provincia"], data["pagina"]])
        return Rpta(True, "OK", lista_tipo_).send()

    except:
        tb = traceback.format_exc()
        print(tb)
        r = Rpta(False, "Ocurrió un error al realizar la consulta de lista de tipos: " + tb, None)
        return r.send()
