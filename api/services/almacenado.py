from django.views.decorators.csrf import csrf_exempt
from willaq.api.utils import Response
from willaq.api.utils import UtilBase
from api.models import tipo
import traceback
from willaq.api.utils import Rpta
from willaq.api.utils import getData

@csrf_exempt
def listararchivos(request):
    try:
        q = getData(tipo)
        data = q.setRequest(request)
        lista_tipo_ = q.querySPArray("p_lista", [])
        return Rpta(True, "OK", lista_tipo_).send()
    except:
        tb = traceback.format_exc()
        print(tb)
        r = Rpta(False, "Ocurrió un error al realizar la consulta de lista de tipos: " + tb, None)
        return r.send()



